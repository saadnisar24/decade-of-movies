## Installation

Clone this repository and import into Android Studio git clone https://bitbucket.org/saadnisar24/decade-of-movies.git

## Requirements

● Loading time of the local data isn’t an issue, searching and sorting times should be optimized.

● You are free to play with any UI elements and kits, unleash the artist within

● You are free to use any libraries or third parties

● Use Github to host your project, and try to avoid committing all your work in one commit “Final Final Project Commit”

● Unit Tests is a must and UI Test is a bonus

● You are free to adopt any architecture you want

● Caching the loaded data to minimize the parsing time is a bonus
Tool and Technlogies

## Tool and Technologies 
● Kotlin

● Android Lifecycle

● MVVM

● Picasso

● Retrofit 2

● Gson

● Repository Pattern

● Junit 4