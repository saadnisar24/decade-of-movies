package com.movies.decadeofmovies.Repository

import Movie
import MoviesList
import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule

class MoviesRepositoryTest {
    private lateinit var moviesRepository: MoviesRepository
    private lateinit var moviesMutableLiveData: MutableLiveData<MoviesList>
    private lateinit var context: Context
    @Before
    fun initialize(){

        moviesRepository= MoviesRepository.getInstance()!!
        context= InstrumentationRegistry.getInstrumentation().targetContext

    }

    @Test
    fun getAllMovies() {
        var result=moviesRepository.getAllMovies(context)
        assertNotNull(result)

    }

    @Test
    fun getFilteredList() {
        var result = moviesRepository.getFilteredList("Avatar")
        assertNotNull(result)
    }
}