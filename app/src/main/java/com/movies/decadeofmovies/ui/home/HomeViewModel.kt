package com.movies.decadeofmovies.ui.home

import MoviesList
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.movies.decadeofmovies.Repository.MoviesRepository

class HomeViewModel : ViewModel() {

    private var moviesMutableLiveData: MutableLiveData<MoviesList>? = null
    private var moviesRepository:MoviesRepository?=null

    fun init(context: Context) {
        if (moviesMutableLiveData != null) {
            return
        }
        moviesRepository = MoviesRepository.getInstance()
        moviesMutableLiveData=MutableLiveData()
        moviesMutableLiveData!!.postValue(moviesRepository?.getAllMovies(context))

    }
    fun getMoviesList(): LiveData<MoviesList?>? {
        return moviesMutableLiveData
    }
}