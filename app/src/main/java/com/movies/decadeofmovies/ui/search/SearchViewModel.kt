package com.movies.decadeofmovies.ui.search

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.movies.decadeofmovies.Repository.MoviesRepository

class SearchViewModel : ViewModel() {

    private var moviesMutableLiveData: MutableLiveData<List<Any>> = MutableLiveData()
    private var moviesRepository: MoviesRepository?=null

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text

    fun init(context: Context) {
        if (moviesRepository != null) {
            return
        }
        moviesRepository = MoviesRepository.getInstance()
        searchMovies("")

    }
    fun searchMovies(string: String)
    {
        moviesMutableLiveData.postValue(moviesRepository?.getFilteredList(string))
    }
    fun getSearchMoviesList(): LiveData<List<Any>> {
        return moviesMutableLiveData
    }
}