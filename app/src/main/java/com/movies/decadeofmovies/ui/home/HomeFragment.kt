package com.movies.decadeofmovies.ui.home

import Movie
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.movies.decadeofmovies.R
import com.movies.decadeofmovies.adapters.MoviesAdapter

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var moviesAdapter: MoviesAdapter? = null
    private var movies:List<Movie>? = null




    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
                ViewModelProvider(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        val rv_movies: RecyclerView = root.findViewById(R.id.rv_movies)



        context?.let { homeViewModel.init(it) }
        homeViewModel.getMoviesList()?.observe(viewLifecycleOwner) { moviesList ->
            movies=moviesList?.movies
            moviesAdapter= MoviesAdapter(movies)
            rv_movies?.layoutManager = LinearLayoutManager(this.context, RecyclerView.VERTICAL, false)
            rv_movies?.itemAnimator = DefaultItemAnimator()
            rv_movies?.adapter = moviesAdapter
        }

//        val helper = Helper(this.requireContext())
//        helper.getMoviesLocal()



        return root
    }
}