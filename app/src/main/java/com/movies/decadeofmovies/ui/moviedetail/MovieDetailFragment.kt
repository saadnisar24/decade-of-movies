package com.movies.decadeofmovies.ui.moviedetail

import BaseImageResponse
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import com.movies.decadeofmovies.Interface.ApiService
import com.movies.decadeofmovies.R
import com.movies.decadeofmovies.Utils.FlickrService
import com.movies.decadeofmovies.adapters.ImagesAdapter
import com.movies.decadeofmovies.ui.moviedetail.MovieDetailFragmentArgs.Companion.fromBundle
import kotlinx.android.synthetic.main.fragment_movie_detail.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MovieDetailFragment : Fragment() {
    val movie by lazy {
        fromBundle(requireArguments()).movie
    }
    var imagesAdapter:ImagesAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_movie_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tv_movie_name.setText(movie!!.title)
        tv_movie_year.setText("(" + movie!!.year + ")")
        tv_movie_Cast.setText(movie!!.cast.toString().replace("[", "").replace("]", ""))
        rb_rating.rating=movie!!.rating.toFloat()
        for (genre in movie!!.genres) {
            chips_view.addItem(genre)
        }
        getImages()
    }


    private fun getImages()
    {
        val service: ApiService? = FlickrService.getRetrofitInstance()?.create(ApiService::class.java)
        service?.getMovieImages(
            resources.getString(R.string.flick_key),// flickr key
            "json",// Response Format
            1, // nojsoncallback
            movie!!.title, // Title of Movie
            1, // page
            10 // perPage
        )?.enqueue(object : Callback<BaseImageResponse> {
            override fun onResponse(
                call: Call<BaseImageResponse>,
                response: Response<BaseImageResponse>
            ) {
                if (response.code() == 200) {
                    imagesAdapter = ImagesAdapter(response.body()!!.photos.photo)
                    rv_images?.itemAnimator = DefaultItemAnimator()
                    rv_images?.adapter = imagesAdapter
                }
            }

            override fun onFailure(call: Call<BaseImageResponse>, t: Throwable) {
                t.stackTrace

            }
        })
    }
}