package com.movies.decadeofmovies.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView.OnQueryTextListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.movies.decadeofmovies.R
import com.movies.decadeofmovies.adapters.SearchMoviesAdapter
import kotlinx.android.synthetic.main.fragment_search.*

class SearchFragment : Fragment() {

    private lateinit var searchViewModel: SearchViewModel
    private lateinit var searchMoviesAdapter: SearchMoviesAdapter
    private var list:List<Any>? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        searchViewModel =
                ViewModelProvider(this).get(SearchViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_search, container, false)
        this.context?.let { searchViewModel.init(it) }
        searchViewModel.getSearchMoviesList()?.observe(viewLifecycleOwner, Observer { moviesList ->

            list = moviesList
            searchMoviesAdapter = SearchMoviesAdapter(list)
            rv_search_movies?.layoutManager = LinearLayoutManager(
                this.context,
                RecyclerView.VERTICAL,
                false
            )
            rv_search_movies?.itemAnimator = DefaultItemAnimator()
            rv_search_movies?.adapter = searchMoviesAdapter
        })

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        searchView.setOnQueryTextListener(
            object : OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    return false
                }

                override fun onQueryTextChange(newText: String): Boolean {
                    searchViewModel.searchMovies(newText)
                    return false
                }
            })
    }
}