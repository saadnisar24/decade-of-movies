package com.movies.decadeofmovies.adapters

import Movie
import Photo
import android.graphics.Color
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

import com.movies.decadeofmovies.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.card_list_image.view.*



class ImagesAdapter(private val list: List<Photo>?) :
        RecyclerView.Adapter<ImagesAdapter.ViewHolder>(){

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val iv_image: ImageView

        init {
            // Define click listener for the ViewHolder's View.
            iv_image = view.findViewById(R.id.iv_image)

        }

    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.card_list_image, viewGroup, false)

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
//        viewHolder.textView.text = list[position]
        val photo:Photo=list!!.get(position)
        val url:String = "http://farm"+photo.farm+".static.flickr.com/"+photo.server+"/"+photo.id+"_"+photo.secret+".jpg"
        Picasso.get().load(url).into(viewHolder.iv_image)
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = list!!.size

}
