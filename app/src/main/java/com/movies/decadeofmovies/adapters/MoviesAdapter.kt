package com.movies.decadeofmovies.adapters

import Movie
import android.graphics.Color
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import androidx.core.view.size
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip

import com.movies.decadeofmovies.R
import com.movies.decadeofmovies.ui.home.HomeFragmentDirections
import com.movies.decadeofmovies.ui.search.SearchFragmentDirections
import kotlinx.android.synthetic.main.fragment_movie_detail.*
import org.tiwu.materialchips.ChipsView


class MoviesAdapter(private val list: List<Movie>?) :
        RecyclerView.Adapter<MoviesAdapter.ViewHolder>(){

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_Name: TextView
        val tv_year: TextView
        val tv_genre: TextView
        val rb_rating: RatingBar
        init {
            // Define click listener for the ViewHolder's View.
            tv_Name = view.findViewById(R.id.tv_Name)
            tv_year = view.findViewById(R.id.tv_year)
            tv_genre = view.findViewById(R.id.tv_genre)
            rb_rating = view.findViewById(R.id.rb_rating)
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.card_list_movie, viewGroup, false)

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        if (list != null) {
            viewHolder.tv_Name.setText(list.get(position).title)
            viewHolder.tv_year.setText(list.get(position).year.toString())
            viewHolder.tv_genre.setText(list.get(position).genres.toString().replace("[", "").replace("]", ""))
            viewHolder.rb_rating.rating=list.get(position).rating.toFloat()
            viewHolder.itemView.setOnClickListener(object : View.OnClickListener {
                override fun onClick(view: View) {
                    val direction = HomeFragmentDirections.actionNavigationHomeToMovieDetailFragment(list.get(position))
                    view.findNavController().navigate(direction)
                }

            })
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = list!!.size

}
