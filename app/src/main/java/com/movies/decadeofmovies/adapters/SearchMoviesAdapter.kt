package com.movies.decadeofmovies.adapters

import Movie
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.Switch
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.movies.decadeofmovies.R
import com.movies.decadeofmovies.ui.search.SearchFragmentDirections
import kotlinx.android.synthetic.main.card_list_movie.view.*
import kotlinx.android.synthetic.main.card_list_year.view.*


class SearchMoviesAdapter(public val list: List<Any>?) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    override fun getItemViewType(position: Int): Int {
        // Just as an example, return 0 or 2 depending on position
        // Note that unlike in ListView adapters, types don't have to be contiguous
        if(list?.get(position) is Int)
        {
            return 0
        }else if(list?.get(position) is Movie)
        {
            return 1
        }
        return position % 2 * 2
    }

    class MoviesViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(movie: Movie) = with(itemView){
            tv_Name.setText(movie.title)
            tv_year.setText(movie.year.toString())
            tv_genre.setText(movie.genres.toString().replace("[", "").replace("]", ""))
            rb_rating.rating=movie.rating.toFloat()

            itemView.setOnClickListener(object : View.OnClickListener {
                override fun onClick(view: View) {
                    val direction =SearchFragmentDirections.actionNavigationSearchToMovieDetailFragment(movie)
                    view.findNavController().navigate(direction)
                }

            })

        }

    }

    class YearViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(year: Int) = with(itemView) {
            tv_year_header.setText(year.toString())
        }

    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        // Create a new view, which defines the UI of the list item
        if (viewType == 1) {
            return MoviesViewHolder(
                LayoutInflater.from(viewGroup.context).inflate(R.layout.card_list_movie, viewGroup, false)
            )
        }
        return YearViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.card_list_year, viewGroup, false)
        )
    }
    override fun getItemCount() = list!!.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            1 -> (holder as MoviesViewHolder).bind(list?.get(position) as Movie)
            0 -> (holder as YearViewHolder).bind(list?.get(position) as Int)
        }
    }

}
