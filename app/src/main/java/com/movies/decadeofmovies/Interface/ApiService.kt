package com.movies.decadeofmovies.Interface

import BaseImageResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("services/rest/?method=flickr.photos.search")
    fun getMovieImages(
        @Query("api_key") apiKey: String,
        @Query("format") format: String = "json",
        @Query("nojsoncallback") callbackNum: Int ,
        @Query("text") title: String,
        @Query("page") page: Int ,
        @Query("per_page") perPage: Int
    ): Call<BaseImageResponse>
}