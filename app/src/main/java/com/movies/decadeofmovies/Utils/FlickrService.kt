package com.movies.decadeofmovies.Utils

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class FlickrService {
   companion object{
       private var retrofit: Retrofit? = null

       fun getRetrofitInstance(): Retrofit? {
           if (retrofit == null) {
               retrofit = Retrofit.Builder()
                   .baseUrl("https://api.flickr.com/")
                   .addConverterFactory(GsonConverterFactory.create())
                   .build()
           }
           return retrofit
       }
   }
}