package com.movies.decadeofmovies.Utils

import MoviesList
import android.content.Context
import android.content.res.AssetManager
import com.google.gson.Gson


class Helper() {

    open  fun getMoviesFromJson(context: Context): MoviesList? {
        val moviesStr = readJSONFromAsset(context)
        val gson = Gson()
        val moviesList: MoviesList = gson.fromJson(moviesStr, MoviesList::class.java)

        return  moviesList
    }

    fun readJSONFromAsset(context: Context): String? {
        val jsonfile: String = context.assets.open("movies.json").bufferedReader().use {it.readText()}
        return jsonfile
    }

    fun AssetManager.readFile(fileName: String) = open(fileName)
        .bufferedReader()
        .use { it.readText() }
        }

    fun AssetManager.readAssetsFile(fileName : String): String = open(fileName).bufferedReader().use{it.readText()}