import com.google.gson.annotations.SerializedName

data class MoviesList (

	@SerializedName("movies") val movies : List<Movie>
)