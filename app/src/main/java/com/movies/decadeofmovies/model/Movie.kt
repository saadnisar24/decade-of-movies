import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
data class Movie (

	@SerializedName("title") val title : String,
	@SerializedName("year") val year : Int,
	@SerializedName("cast") val cast : List<String>,

	@SerializedName("genres") val genres : List<String>,
	@SerializedName("rating") val rating : Int

):Comparable<Movie>,Parcelable{
	constructor(parcel: Parcel) : this(
		parcel.readString()!!,
		parcel.readInt(),
		parcel.createStringArrayList()!!,
		parcel.createStringArrayList()!!,
		parcel.readInt()
	) {
	}

	override fun compareTo(other: Movie): Int {
		if (year == null || other.year == null) {
			return 0
		}
		return year.compareTo(other.year);

	}

	override fun describeContents(): Int {
		TODO("Not yet implemented")
	}

	override fun writeToParcel(p0: Parcel?, p1: Int) {
		TODO("Not yet implemented")
	}

	companion object CREATOR : Parcelable.Creator<Movie> {
		override fun createFromParcel(parcel: Parcel): Movie {
			return Movie(parcel)
		}

		override fun newArray(size: Int): Array<Movie?> {
			return arrayOfNulls(size)
		}
	}


}