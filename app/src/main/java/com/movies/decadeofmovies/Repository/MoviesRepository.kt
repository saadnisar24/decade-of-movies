package com.movies.decadeofmovies.Repository

import Movie
import MoviesList
import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.movies.decadeofmovies.Utils.Helper
import java.util.*
import java.util.Collections.sort
import java.util.stream.Collectors
import kotlin.collections.ArrayList


class MoviesRepository {
    companion object {
        private var moviesRepository: MoviesRepository? = null
        private var helper: Helper? = null
//        var movies:MutableLiveData<Movies>?=null
        var moviesList: MoviesList? = null
        var moviesHashmap = HashMap<Int, Movie>()
        lateinit var filteredList:List<Movie>

        fun getInstance(): MoviesRepository? {
            if (moviesRepository == null) {
                moviesRepository =
                    MoviesRepository()
                helper=Helper();
            }
            return moviesRepository
        }
    }


    fun getAllMovies(context: Context):MoviesList?  {
        moviesList =helper?.getMoviesFromJson(context)
        return moviesList
    }


    fun getFilteredList(enteredWord:String):List<Any>
    {
        if (!enteredWord.isEmpty()){
            filteredList= moviesList?.movies!!.filter { it.title.toLowerCase().contains(enteredWord.toLowerCase()) }
        }else{
            filteredList= moviesList?.movies!!
        }

       sort(filteredList);
        Collections.reverse(filteredList);
        var movies = filteredList.groupBy { (it.year) }.flatMap {
            var list = it.value.sortedByDescending { movie ->
                movie.rating
            }.toMutableList<Any>()
            if (list.size > 5)
                list = list.subList(0, 5)
            // append the year value
            list.add(0, it.key)
            list

        }
        return movies
    }





}